package flyweight.main;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import flyweight.explorer.FileExplorer;

public class Runner {
	public static void main(String args[]) throws IOException {
		String directory = System.getProperty("user.dir");
		FileExplorer explorer = new FileExplorer(directory);
		explorer.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				System.exit(0);
			}
		});
		explorer.setVisible(true);
	}
}

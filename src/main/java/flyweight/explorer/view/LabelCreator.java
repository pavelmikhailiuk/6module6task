package flyweight.explorer.view;

import java.io.File;

import javax.swing.JLabel;

public interface LabelCreator {
	void setCurrentDirectory(File directory);

	JLabel create(String name);
}

package flyweight.explorer.view.impl;

import java.awt.event.MouseListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import flyweight.explorer.view.LabelCreator;

public class LabelCreatorImpl implements LabelCreator {
	private MouseListener listener;
	private Map<String, JLabel> labels = new HashMap<String, JLabel>();
	private File currentDirectory;

	public LabelCreatorImpl(MouseListener listener) {
		this.listener = listener;
	}

	public JLabel create(String name) {
		File file = new File(currentDirectory, name);
		if (!file.exists()) {
			throw new IllegalArgumentException(name + " no such file or directory");
		}
		JLabel label = labels.get(name);
		if (label == null) {
			if (file.isDirectory()) {
				label = new JLabel(name, new ImageIcon("folder.png"), JLabel.TRAILING);
				label.addMouseListener(listener);
			} else {
				label = new JLabel(name, new ImageIcon("file.png"), JLabel.TRAILING);
			}
			labels.put(name, label);
		}
		return label;

	}

	public void setCurrentDirectory(File currentDirectory) {
		this.currentDirectory = currentDirectory;
	}
}

package flyweight.explorer;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Arrays;

import javax.swing.JLabel;

import flyweight.explorer.view.LabelCreator;
import flyweight.explorer.view.impl.LabelCreatorImpl;

public class FileExplorer extends Frame implements ActionListener {
	private LabelCreator creator;
	private TextField currentPath;
	private Panel buttons;
	private Button up;
	private Button close;
	private File currentDir;
	private String[] files;

	public FileExplorer(String directory) {
		creator = new LabelCreatorImpl(addListener());
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});
		createView();
		listDirectory(directory);
	}

	public void listDirectory(String directory) {
		File currentDirectory = new File(directory);
		if (!currentDirectory.isDirectory()) {
			throw new IllegalArgumentException("No such directory");
		}
		files = currentDirectory.list();
		Arrays.sort(files);
		setLayout(new FlowLayout());
		creator.setCurrentDirectory(currentDirectory);
		for (int i = 0; i < files.length; i++) {
			add(creator.create(files[i]));
		}
		this.setTitle(directory);
		currentPath.setText(directory);
		currentDir = currentDirectory;
	}

	private void createView() {
		createTextField();
		createButtonsPanel();
		this.add(currentPath);
		this.add(buttons);
		this.setSize(800, 500);
	}

	private void createTextField() {
		currentPath = new TextField();
		currentPath.setFont(new Font("MonoSpaced", Font.PLAIN, 12));
		currentPath.setEditable(false);
	}

	private void createButtonsPanel() {
		buttons = new Panel();
		buttons.setLayout(new FlowLayout(FlowLayout.RIGHT, 15, 5));
		buttons.setFont(new Font("SansSerif", Font.BOLD, 14));

		up = new Button("Up a Directory");
		close = new Button("Close");
		up.addActionListener(this);
		close.addActionListener(this);
		buttons.add(up);
		buttons.add(close);
	}

	private MouseListener addListener() {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent me) {
				JLabel label = (JLabel) me.getSource();
				down(label.getText());
			}
		};
	}

	private void up() {
		String parent = currentDir.getParent();
		if (parent != null) {
			pack();
			removeAll();
			createView();
			listDirectory(parent);
		}
	}

	private void down(String name) {
		if (name != null) {
			pack();
			removeAll();
			createView();
			listDirectory(name);
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == close)
			this.dispose();
		else if (e.getSource() == up) {
			up();
		}
	}
}
